@extends('layout')

@section('content')


<div class="card mt-5">
    <div class="card-header">
        <h3>Update User
        <a href="{{ url('usermasters') }}" class="btn btn-danger float-end">BACK</a>
        </h3>
    </div>

    <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form method="post" action="{{ route('usermasters.update', $usermaster->id) }}">
            <div class="form-group">
                @csrf
                @method('PATCH')
                <label for="name">Name</label>
                <input type="text" class="form-control" name="name" value="{{ $usermaster->name }}" />
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <input type="text" class="form-control" name="description" value="{{ $usermaster->description }}" />
            </div>
            <div class="form-group">
                <label for="">User Profile Image</label>
                <input type="file" class="form-control" name="profile_image">
                <img src="{{ asset('images/'.$usermaster->profile_image) }}" width="70px" height="70px">
            </div>


            <button type="submit" class="btn btn-success">Update</button>
        </form>
    </div>
</div>
@endsection