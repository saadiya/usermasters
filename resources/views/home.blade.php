@extends('layout')

@section('content')

<div class="card mt-5">
  <div class="card-header">
    <h3>Add New User
      <a href="{{ url('usermasters') }}" class="btn btn-danger float-end">BACK</a>
    </h3>
  </div>

  <div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div>
    @endif
      <form method="post" action="{{ route('usermasters.store') }}">
          <div class="form-group">
              @csrf
              <label for="name">Name</label>
              <input type="text" class="form-control" name="name"/>
          </div>
          <div class="form-group">
              <label for="description">Description</label>
              <input type="text" class="form-control" name="description"/>
          </div>
          <div class="form-group">
              <label for="profile_image">Profile_image</label>
              <input type="file" class="form-control" name="profile_image"/>
          </div>
          <button type="submit" class="btn btn-success">Save</button>
      </form>
  </div>
</div>
@endsection