@extends('layout')

@section('content')

<div class="mt-5">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
  <h1 style="text-align:center">User Details</h1>
  <a style="float:right" href="http://127.0.0.1:8000/home"><button class="btn btn-success btn-sm">Add New</button></a><br />
  <table class="table" width="100%" border="1px">
    <thead>
        <tr class="table-primary text-center">
          <td>S No.</td>
          <td>Name</td>
          <td>Description</td>
          <td>Image</td>
          <td>Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($usermaster as $usermaster)
        <tr class="text-center">
            <td>{{$usermaster->id}}</td>
            <td>{{$usermaster->name}}</td>
            <td>{{$usermaster->description}}</td>
            <td>
                <img src="{{ asset('images/'.$usermaster->profile_image) }}" width="70px" height="70px">
            </td>
            <td>
              <a href="{{ route('usermasters.edit', $usermaster->id)}}" class="btn btn-primary btn-sm">Edit</a>
              <form action="{{ route('usermasters.destroy', $usermaster->id)}}" method="post" style="display: inline-block">
                @csrf
                @method('DELETE')
                <button class="btn btn-danger btn-sm" type="submit">Delete</button>
              </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection