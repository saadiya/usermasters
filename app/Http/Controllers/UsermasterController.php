<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usermaster;
use Illuminate\Support\Facades\File;

class UsermasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $usermaster= Usermaster::all();
        return view('index', compact('usermaster'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usermaster = new Usermaster;
        $usermaster->name = $request->input('name');
        $usermaster->description = $request->input('description');
        if($request->hasfile('profile_image'))
        {
            $file = $request->file('profile_image');
            $extention = $file->getClientOriginalExtension();
            $filename = time().'.'.$extention;
            $file->move('images/', $filename);
            $usermaster->profile_image = $filename;
        }
        $usermaster->save();
        return redirect()->back()->with('status','User details Added Successfully');
    }

        

        

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $usermaster = Usermaster::findOrFail($id);
        return view('update', compact('usermaster'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $usermaster = Usermaster::find($id);
        $usermaster->name = $request->input('name');
        $usermaster->description = $request->input('description');

        if($request->hasfile('profile_image'))
        {
            $destination = 'images/'.$usermaster->profile_image;
            if(File::exists($destination))
            {
                File::delete($destination);
            }
            $file = $request->file('profile_image');
            $extention = $file->getClientOriginalExtension();
            $filename = time().'.'.$extention;
            $file->move('images/', $filename);
            $usermaster->profile_image = $filename;
        }

        $usermaster->update();
        return redirect()->back()->with('status','User Details Updated Successfully');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $usermaster = Usermaster::findOrFail($id);
        $destination = 'images/'.$usermaster->profile_image;
        if(File::exists($destination))
        {
            File::delete($destination);
        }
        $usermaster->delete();

        return redirect('/usermasters')->with('completed', 'usermaster deleted');
        //return redirect()->back()->with('status','User Image Deleted Successfully');
    }
}
